<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library("session");
    }

    public function index()
    {

        $this->load->model("posts");
        $posts = $this->posts->findAll();

        $data = array(
            "scripts" => array(
                "owl.carousel.min.js",
                "theme-scripts.js"
            ),
            "posts" => $posts,
        );

        $this->template->show("home.php", $data);
    }
}
