$(function(){

    $("#btn_add_post").click(function(){
        clearErrors();
        $("#form_post")[0].reset();
        $("#post_img_path").attr("src", "");
        $("#modal_post").modal();
    });

    $("#btn_add_membro").click(function(){
        clearErrors();
        $("#form_membro")[0].reset();
        $("#membro_img_path").attr("src", "");
        $("#modal_membro").modal();
    });

    $("#membro_estado").change(function(){
        
        var uf_estado = $(this).val();

        $.post(BASE_URL + "admin/getCidades", {
            uf_estado : uf_estado
        }, function(response){
            $('#membro_cidade').html(response);
            $("#membro_cidade").parent().prop("hidden", false);
            $("#membro_cidade").removeAttr("disabled");

        })

    });

    $("#btn_add_user").click(function(){
        clearErrors();
        $("#form_user")[0].reset();
        $("#modal_user").modal();
    });

    $("#btn_upload_post_img").change(function(){
        uploadImg($(this), $("#post_img_path"), $("#post_image"));
    });

    $("#btn_upload_membro_img").change(function(){
        uploadImg($(this), $("#membro_img_path"), $("#membro_image"));
    });

    $("#btn_perfil").click(function(){
        $("#modal_perfil").modal();
    })

    $("#form_post").submit(function() {

        $.ajax({
            type: "POST",
            url: BASE_URL + "admin/ajax_save_post",
            dataType: "json",
            data: $(this).serialize(),
            beforeSend: function() {
                clearErrors();
                $("#btn_save_post").siblings(".help-block").html(loadingImg("Verificando..."));
            },
            success: function(response) {
                clearErrors();
                if (response["status"]) {
                    console.log("Response status: "+response["status"]);
                    $("#modal_post").modal("hide");
                    dt_posts.ajax.reload();
                    Swal.fire({
                        position: 'top-end',
                        type: 'success',
                        title: 'O post foi salvo com sucesso!',
                        showConfirmButton: false,
                        timer: 1500
                      });

                } else {
                    showErrorsModal(response["error_list"])
                }
            }
        })

        return false;
    });

    $("#form_membro").submit(function() {

        $.ajax({
            type: "POST",
            url: BASE_URL + "admin/ajax_save_membro",
            dataType: "json",
            data: $(this).serialize(),
            beforeSend: function() {
                clearErrors();
                $("#btn_save_membro").siblings(".help-block").html(loadingImg("Verificando..."));
            },
            success: function(response) {
                clearErrors();
                if (response["status"]) {
                    console.log("Response status: " + response["status"]);
                    $("#modal_membro").modal("hide");
                    dt_time.ajax.reload();
                    Swal.fire({
                        position: 'top-end',
                        type: 'success',
                        title: 'O membro foi salvo com sucesso!',
                        showConfirmButton: false,
                        timer: 1500
                      });
                } else {
                    showErrorsModal(response["error_list"])
                }
            }
        })

        return false;
    });

    $("#form_user").submit(function() {

        $.ajax({
            type: "POST",
            url: BASE_URL + "admin/ajax_save_user",
            dataType: "json",
            data: $(this).serialize(),
            beforeSend: function() {
                clearErrors();
                $("#btn_save_user").siblings(".help-block").html(loadingImg("Verificando..."));
            },
            success: function(response) {
                clearErrors();
                if (response["status"]) {
                    console.log("Response status: " + response["status"]);
                    $("#modal_user").modal("hide");
                    dt_users.ajax.reload();
                    Swal.fire({
                        position: 'top-end',
                        type: 'success',
                        title: 'O usuário foi salvo com sucesso!',
                        showConfirmButton: false,
                        timer: 1500
                      });
                    
                } else {
                    showErrorsModal(response["error_list"])
                }
            }
        })

        return false;
    });

    $("#btn_logged_user").click(function() {

        $.ajax({
            type: "POST",
            url: BASE_URL + "admin/ajax_get_user_data",
            dataType: "json",
            data: {"user_id": $(this).attr("user_id")},
            success: function(response) {
                clearErrors();
                $("#form_user")[0].reset();
                $.each(response["input"], function(id, value) {
                    $("#"+id).val(value);
                });
                $("#modal_user").modal();
            }
        })

        return false;
    });

    function active_btn_post() {
        $(".btn-edit-post").click(function(){
            $.ajax({
                type: "POST",
                url: BASE_URL + "admin/ajax_get_post_data",
                dataType: "json",
                data: {"post_id": $(this).attr("post_id")},
                success: function(response) {
                    clearErrors();
                    $("#form_post")[0].reset();
                    $.each(response["input"], function(id, value) {
                        $("#"+id).val(value);
                    });
                    $("#post_img_path").attr("src", BASE_URL+response["image"]["post_image"]);
                    $("#modal_post").modal();
                }
            })
        })

        $(".btn-del-post").click(function(){
            
            post_id = $(this);

            Swal.fire({
                title: "Atenção!",
                text: "Deseja excluir este post?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d9534f",
                confirmButtonText: "Sim",
                cancelButtonText: "Não",
                // closeOnConfirm: true,
                // closeOnCancel: true,
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + "admin/ajax_delete_post_data",
                        dataType: "json",
                        data: {"post_id": post_id.attr("post_id")},
                        success: function(response) {
                            Swal.fire({
                                position: 'top-end',
                                type: 'success',
                                title: 'O post foi deletado com sucesso!',
                                showConfirmButton: false,
                                timer: 1500
                              });
                              dt_posts.ajax.reload();
                        }
                    })
                }
            })

        })
    }

    var dt_posts = $("#dt_posts").DataTable({
        "oLanguage": DATATABLE_PTBR,
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL + "admin/ajax_list_posts",
            "type": "POST",
        },
        "columnDefs": [
            { targets: "no-sort", orderable: false },
            { targets: "dt-center", className: "dt-center" },
        ],
        "drawCallback": function() {
            active_btn_post();
        }
    })

    function active_btn_membro() {
        $(".btn-edit-membro").click(function(){
            $.ajax({
                type: "POST",
                url: BASE_URL + "admin/ajax_get_membro_data",
                dataType: "json",
                data: {"membro_id": $(this).attr("membro_id")},
                success: function(response) {
                    clearErrors();
                    $("#form_membro")[0].reset();
                    $.each(response["input"], function(id, value) {
                        $("#"+id).val(value);
                    });
                    $("#membro_img_path").attr("src", BASE_URL+response["image"]["membro_image"]);
                    $("#modal_membro").modal();
                }
            })
        })

        $(".btn-del-membro").click(function(){
            
            membro_id = $(this);

            Swal.fire({
                title: "Atenção!",
                text: "Deseja excluir este membro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d9534f",
                confirmButtonText: "Sim",
                cancelButtonText: "Não",
                // closeOnConfirm: true,
                // closeOnCancel: true,
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + "admin/ajax_delete_membro_data",
                        dataType: "json",
                        data: {"membro_id": membro_id.attr("membro_id")},
                        success: function(response) {
                            Swal.fire({
                                position: 'top-end',
                                type: 'success',
                                title: 'O membro foi deletado com sucesso!',
                                showConfirmButton: false,
                                timer: 1500
                              });
                              dt_time.ajax.reload();
                        }
                    })
                }
            })

        })
    }

    var dt_time = $("#dt_time").DataTable({
        "oLanguage": DATATABLE_PTBR,
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL + "admin/ajax_list_membro",
            "type": "POST",
        },
        "columnDefs": [
            { targets: "no-sort", orderable: false },
            { targets: "dt-center", className: "dt-center" },
        ],
        "drawCallback": function() {
            active_btn_membro();
        }
    })

    function active_btn_user() {
        $(".btn-edit-user").click(function(){
            $.ajax({
                type: "POST",
                url: BASE_URL + "admin/ajax_get_user_data",
                dataType: "json",
                data: {"user_id": $(this).attr("user_id")},
                success: function(response) {
                    clearErrors();
                    $("#form_user")[0].reset();
                    $.each(response["input"], function(id, value) {
                        $("#"+id).val(value);
                    });
                    $("#modal_user").modal();
                }
            })
        })

        $(".btn-del-user").click(function(){
            
            user_id = $(this);

            Swal.fire({
                title: "Atenção!",
                text: "Deseja excluir este usuário?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d9534f",
                confirmButtonText: "Sim",
                cancelButtonText: "Não",
                // closeOnConfirm: true,
                // closeOnCancel: true,
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + "admin/ajax_delete_user_data",
                        dataType: "json",
                        data: {"user_id": user_id.attr("user_id")},
                        success: function(response) {
                            Swal.fire({
                                position: 'top-end',
                                type: 'success',
                                title: 'O usuário foi deletado com sucesso!',
                                showConfirmButton: false,
                                timer: 1500
                              });
                              dt_users.ajax.reload();
                        }
                    })
                }
            })

        })
    }

    var dt_users = $("#dt_users").DataTable({
        "oLanguage": DATATABLE_PTBR,
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL + "admin/ajax_list_users",
            "type": "POST",
        },
        "columnDefs": [
            { targets: "no-sort", orderable: false },
            { targets: "dt-center", className: "dt-center" },
        ],
        "drawCallback": function() {
            active_btn_user();
        }
    })

})