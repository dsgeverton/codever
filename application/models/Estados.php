<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Estados extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getAll() {
        $this->db->from("Estado")->order_by("Nome");
        return $this->db->get();
    }

    public function selectEstados() {
        $options = "<option value=''>Selecione o Estado</option>";
        $estados = $this->getAll();
        foreach($estados -> result() as $estado) {
            $options .= "<option value='{$estado->Uf}'>{$estado->Uf}</option>";
        }

        return $options;
    }
}