<section style="min-height: calc(100vh - 83px);" class="light-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-offset-3 col-lg-6 text-center">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="section-title">
                            <h2>Área do Administrador</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-offset-5 col-lg-2 text-center">
                <div class="form-group">
                    <a id="btn_logged_user" class="btn btn-link" user_id="<?=$user_id?>"><i class="fa fa-user"></i></a>
                    <a href="admin/logoff" class="btn btn-link"><i class="fa fa-sign-out"></i></a>
                </div>
            </div>
        </div>

        <div class="container">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_posts" role="tab" data-toggle="tab">Posts</a></li>
                <li><a href="#tab_time" role="tab" data-toggle="tab">Time</a></li>
                <li><a href="#tab_galeria" role="tab" data-toggle="tab">Galeria</a></li>
                <li><a href="#tab_usuarios" role="tab" data-toggle="tab">Usuários</a></li>
            </ul>

            <div class="tab-content">
                <div id="tab_posts" class="tab-pane active">
                    <div class="container-fluid">
                        <h2 class="text-center"><strong>Gerenciar Posts</strong></h2>
                        <a id="btn_add_post" class="btn btn-info"><i class="fa fa-plus">&nbsp;&nbsp;Adicionar</i></a>
                        <table id="dt_posts" class="table table-striped table-bordered">
                            <thead>
                                <tr class="tableheader">
                                    <th class="dt-center">Título</th>
                                    <th class="dt-center no-sort">Imagem</th>
                                    <th class="">Descrição</th>
                                    <th class="dt-center no-sort">Ações</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="tab_time" class="tab-pane">
                    <div class="container-fluid">
                        <h2 class="text-center"><strong>Gerenciar Time</strong></h2>
                        <a id="btn_add_membro" class="btn btn-info"><i class="fa fa-plus">&nbsp;&nbsp;Adicionar</i></a>
                        <table id="dt_time" class="table table-striped table-bordered">
                            <thead>
                                <tr class="tableheader">
                                    <th class="dt-center">Nome</th>
                                    <th class="dt-center no-sort">Foto</th>
                                    <th class="dt-center">Cidade</th>
                                    <th class="dt-center">Cargo</th>
                                    <th class="">Descrição</th>
                                    <th class="dt-center no-sort">Ações</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="tab_galeria" class="tab-pane">
                    <div class="container-fluid">
                        <h2 class="text-center"><strong>Gerenciar Galeria</strong></h2>
                        <a id="btn_add_item" class="btn btn-info"><i class="fa fa-plus">&nbsp;&nbsp;Adicionar</i></a>
                    </div>
                </div>
                <div id="tab_usuarios" class="tab-pane">
                    <div class="container-fluid">
                        <h2 class="text-center"><strong>Gerenciar Usuários</strong></h2>
                        <a id="btn_add_user" class="btn btn-info"><i class="fa fa-plus">&nbsp;&nbsp;Adicionar</i></a>
                        <table id="dt_users" class="table table-striped table-bordered">
                            <thead>
                                <tr class="tableheader">
                                    <th class="dt-center">Login</th>
                                    <th class="dt-center">Nome</th>
                                    <th class="dt-center">Email</th>
                                    <th class="dt-center no-sort">Ações</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>


<div id="modal_post" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">Posts</h4>
            </div>
            <div class="modal-body">
                <form action="" id="form_post">
                    <input id="post_id" name="post_id" hidden>
                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Título</label>
                        <div class="col-lg-10">
                            <input id="post_titulo" name="post_titulo" class="form-control" maxlenght="100">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Imagem</label>
                        <div class="col-lg-10 text-center">
                            <img id="post_img_path" name="post_img_path" src="" style="max-width: 200px; max-height: 200px">
                            <label class="btn btn-block btn-info">
                                <i class="fa fa-upload"></i>&nbsp;&nbsp;Importar Imagem
                                <input type="file" id="btn_upload_post_img" name="btn_upload_post_img" accept="image/*" style="display: none;">
                            </label>
                            <input hidden id="post_image" name="post_image">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Descrição</label>
                        <div class="col-lg-10">
                            <textarea id="post_descricao" name="post_descricao" class="form-control"></textarea>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" id="btn_save_post" name="btn_save_post" class="btn btn-primary">
                            <i class="fa fa-save"></i>&nbsp;&nbsp;Salvar
                        </button>
                        <span class="help-block"></span>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<div id="modal_membro" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">Membro do Time</h4>
            </div>
            <div class="modal-body">
                <form action="" id="form_membro">
                    <input id="membro_id" name="membro_id" hidden>
                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Nome</label>
                        <div class="col-lg-10">
                            <input id="membro_nome" name="membro_nome" class="form-control" maxlenght="100">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Foto</label>
                        <div class="col-lg-10 text-center">
                            <img id="membro_img_path" name="membro_img_path" src="" alt="" style="max-width: 200px; max-height: 200px">
                            <label class="btn btn-block btn-info">
                                <i class="fa fa-upload"></i>&nbsp;&nbsp;
                                <input type="file" name="btn_upload_membro_img" id="btn_upload_membro_img" accept="image/*" style="display: none;">
                            </label>
                            <input hidden id="membro_image" name="membro_image">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Cidade</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select class="form-control" name="membro_estado" id="membro_estado">
                                    <?php echo $options_estados; ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control" id="membro_cidade" name="membro_cidade" disabled>
                                    <option>Selecione o estado</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Cargo</label>
                        <div class="col-lg-10">
                            <input id="membro_cargo" name="membro_cargo" class="form-control" maxlenght="100">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Descrição</label>
                        <div class="col-lg-10">
                            <textarea id="membro_descricao" name="membro_descricao" class="form-control"></textarea>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" id="btn_save_membro" name="btn_save_membro" class="btn btn-primary">
                            <i class="fa fa-save"></i>&nbsp;&nbsp;Salvar
                        </button>
                        <span class="help-block"></span>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<div id="modal_user" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">Usuário</h4>
            </div>
            <div class="modal-body">
                <form action="" id="form_user">
                    <input id="user_id" name="user_id" hidden>
                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Login</label>
                        <div class="col-lg-10">
                            <input id="user_login" name="user_login" class="form-control" maxlenght="50">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Nome</label>
                        <div class="col-lg-10">
                            <input id="user_nome" name="user_nome" class="form-control" maxlenght="50">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Email</label>
                        <div class="col-lg-10">
                            <input id="user_email" name="user_email" class="form-control" maxlenght="100">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Confirmar Email</label>
                        <div class="col-lg-10">
                            <input id="user_confirm_email" name="user_confirm_email" class="form-control" maxlenght="100">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Senha</label>
                        <div class="col-lg-10">
                            <input id="user_password" type="password" name="user_password" class="form-control">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-lg-2 control-label">Confirmar Senha</label>
                        <div class="col-lg-10">
                            <input id="user_confirm_password" type="password" name="user_confirm_password" class="form-control">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" id="btn_save_user" name="btn_save_user" class="btn btn-primary">
                            <i class="fa fa-save"></i>&nbsp;&nbsp;Salvar
                        </button>
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
