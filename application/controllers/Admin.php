<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("session");
    }

    public function index()
    {
        if ($this->session->userdata("user_id")){
            $this->load->model("estados");
            $data = array(
                "styles" => array(
                    "dataTables.bootstrap.min.css",
                    "datatables.min.css"
                ),
                "scripts" => array(
                    "sweetalert2.all.min.js",
                    "dataTables.bootstrap.min.js",
                    "datatables.min.js",
                    "util.js",
                    "admin.js"
                ),
                "options_estados" => $this->estados->selectEstados(),
                "user_id" => $this->session->userdata("user_id")
            );
            $this->template->show("admin.php", $data);
        } else {
            $data = array(
                "scripts" => array(
                    "util.js",
                    "login.js"
                )
            );
            $this->template->show("login.php", $data);
        }
    }

    public function logoff() {
        $this->session->sess_destroy();
        header("Location: " . base_url() . "admin");
    }

    public function ajax_login() {

        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;
        $json["error_list"] = array();

        $username = $this->input->post("username");
        $password = $this->input->post("password");

        if (empty($username)) {
            $json["status"] = 0;
            $json["error_list"]["#username"] = "Usuário deve ser preenchido!";
        } else {
            $this->load->model("users");
            $result = $this->users->get_user_data($username);
            if ($result) {
                $user_id = $result->user_id;
                $user_password_hash = $result->user_password_hash;
                if (password_verify($password, $user_password_hash)) {
                    $this->session->set_userdata("user_id", $user_id);
                } else {
                    $json["status"] = 0;
                }
            } else {
                $json["status"] = 0;
            }
            if ($json["status"] == 0) {
                $json["error_list"]["#btn_login"] = "Usuário e/ou senha incorretos!";
            }
        }

        echo json_encode($json);
    }

    public function ajax_import_image() {
        
        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $config["upload_path"] = "./tmp/";
        $config["allowed_types"] = "gif|png|jpg";
        $config["overwrite"] = TRUE;

        $this->load->library("upload", $config);

        $json = array();
        $json["status"] = 1;

        if (!$this->upload->do_upload("image_file")) {
            $json["status"] = 0;
            $json["error"] = $this->upload->display_errors("","");
        } else {
            if ($this->upload->data()["file_size"] <= 1024) {
                $file_name = $this->upload->data()["file_name"];
                $json["img_path"] = base_url() . "tmp/" . $file_name;
            } else {
                $json["status"] = 0;
                $json["error"] = "O arquivo não pode ser maior que 1MB!";
            }
        }
        
        echo json_encode($json);
    }

    public function ajax_save_post() {

        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;
        $json["error_list"] = array();

        $this->load->model("posts");

        $data = $this->input->post();

        if (empty($data["post_titulo"])) {
            $json["error_list"]["#post_titulo"] = "Título do post é obrigatório!";
        } else {
            if ($this->posts->is_duplicated("post_titulo", $data["post_titulo"], $data["post_id"])) {
                $json["error_list"]["#post_titulo"] = "Título do post já existe!";
            }
        }

        if (empty($data["post_descricao"])) {
            $json["error_list"]["#post_descricao"] = "Descrição do post é obrigatório!";
        }

        if (!empty($json["error_list"])) {
            $json["status"] = 0;
        } else {
            if (!empty($data["post_image"])) {

                $file_name = basename($data["post_image"]);
                $old_path = getcwd() . "/tmp/" . $file_name;
                $new_path = getcwd() . "/public/images/posts/" . $file_name;
                rename($old_path, $new_path);

                $data["post_image"] = "/public/images/posts/" . $file_name;
            } else {
				unset($data["post_image"]);
			}

            if (empty($data["post_id"])) {
                $this->posts->insert($data);
            } else {
                $post_id = $data["post_id"];
                unset($data["post_id"]);
                $this->posts->update($post_id, $data);
            }
        }

        echo json_encode($json);
    }

    public function ajax_save_membro() {

        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;
        $json["error_list"] = array();

        $this->load->model("time");

        $data = $this->input->post();

        if (empty($data["membro_nome"])) {
            $json["error_list"]["#membro_nome"] = "Nome do membro é obrigatório!";
        } 

        if (empty($data["membro_cargo"])) {
            $json["error_list"]["#membro_cargo"] = "O cargo deve ser informado!";
        }

        if (empty($data["membro_descricao"])) {
            $json["error_list"]["#membro_descricao"] = "Descreva um breve resumo das características desse membro.";
        }

        if (!empty($json["error_list"])) {
            $json["status"] = 0;
        } else {
            if (!empty($data["membro_image"])) {

                $file_name = basename($data["membro_image"]);
                $old_path = getcwd() . "/tmp/" . $file_name;
                $new_path = getcwd() . "/public/images/time/" . $file_name;
                rename($old_path, $new_path);

                $data["membro_image"] = "/public/images/time/" . $file_name;
            } else {
				unset($data["membro_image"]);
            }
            
            unset($data["membro_estado"]);

            if (empty($data["membro_id"])) {
                $this->time->insert($data);
            } else {
                $membro_id = $data["membro_id"];
                unset($data["membro_id"]);
                $this->time->update($membro_id, $data);
            }
        }

        echo json_encode($json);
    }

    public function ajax_save_user() {

        $REG_EXP = "/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/";

        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;
        $json["error_list"] = array();

        $this->load->model("users");

        $data = $this->input->post();

        if (empty($data["user_login"])) {
            $json["error_list"]["#user_login"] = "login é obrigatório!";
        } else {
            if ($this->users->is_duplicated("user_login", $data["user_login"], $data["user_id"])) {
                $json["error_list"]["#user_login"] = "Login já existe!";
            }
        }

        if (empty($data["user_nome"])) {
            $json["error_list"]["#user_nome"] = "Nome do usuário é obrigatório!";
        }

        if (empty($data["user_email"])) {
            $json["error_list"]["#user_email"] = "Endereço de email é obrigatório!";
        } else {
            if ($this->users->is_duplicated("user_email", $data["user_email"], $data["user_id"])) {
                $json["error_list"]["#user_email"] = "Este email já está em uso!";
            }
        } 

        if (empty($data["user_confirm_email"])) {
            $json["error_list"]["#user_confirm_email"] = "Endereço de confirmação de email é obrigatório!";
        } else {
            if (strcmp($data["user_email"], $data["user_confirm_email"])) {
                $json["error_list"]["#user_email"] = "";
                $json["error_list"]["#user_confirm_email"] = "Endereços de email não confirmam!";
            }
        }

        if (empty($data["user_password"])) {
            $json["error_list"]["#user_password"] = "A senha é obrigatória!";
        } else {
            if (!preg_match($REG_EXP, $data["user_password"]) ) {
                $json["error_list"]["#user_password"] = "A senha deve ter no mínimo 8 caracteres e deve conter pelo menos uma letra minúscula, uma letra maiúscula e um algarismo!";
            }
        }

        if (empty($data["user_confirm_password"])) {
            $json["error_list"]["#user_confirm_password"] = "Corfirmação de senha é obrigatório!";
        } else {
            if (strcmp($data["user_password"], $data["user_confirm_password"])) {
                $json["error_list"]["#user_confirm_password"] = "As senhas devem ser iguais!";
            }
        }

        if (!empty($json["error_list"])) {
            $json["status"] = 0;
        } else {

            $data["user_password_hash"] = password_hash($data["user_password"], PASSWORD_DEFAULT);
            $data["user_full_name"] = $data["user_nome"];
            unset($data["user_password"]);
            unset($data["user_confirm_password"]);
            unset($data["user_confirm_email"]);
            unset($data["user_nome"]);

            if (empty($data["user_id"])) {
                $this->users->insert($data);
            } else {
                $user_id = $data["user_id"];
                unset($data["user_id"]);
                $this->users->update($user_id, $data);
            }
        }

        echo json_encode($json);
    }

    public function ajax_delete_post_data() {

        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;

        $this->load->model("posts");
        $post_id = $this->input->post("post_id");
        $data = $this->posts->delete($post_id);

        echo json_encode($json);
    }

    public function ajax_delete_membro_data() {

        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;

        $this->load->model("time");
        $membro_id = $this->input->post("membro_id");
        $data = $this->time->delete($membro_id);

        echo json_encode($json);
    }

    public function ajax_delete_user_data() {

        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;

        $this->load->model("users");
        $user_id = $this->input->post("user_id");
        $data = $this->users->delete($user_id);

        echo json_encode($json);
    }

    public function ajax_get_user_data() {

        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;
        $json["input"] = array();

        $this->load->model("users");

        $user_id = $this->input->post("user_id");
        $data = $this->users->get_data($user_id)->result_array()[0];

        $json["input"]["user_id"] = $data["user_id"];
        $json["input"]["user_login"] = $data["user_login"];
        $json["input"]["user_nome"] = $data["user_full_name"];
        $json["input"]["user_email"] = $data["user_email"];
        $json["input"]["user_confirm_email"] = $data["user_email"];

        // transform password hash in default passwd.
        
        $json["input"]["user_password"] = $data["user_password_hash"];
        $json["input"]["user_confirm_password"] = $data["user_password_hash"];

        echo json_encode($json);
    }

    public function ajax_get_post_data() {

        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;
        $json["input"] = array();

        $this->load->model("posts");

        $post_id = $this->input->post("post_id");
        $data = $this->posts->get_data($post_id)->result_array()[0];

        $json["input"]["post_id"] = $data["post_id"];
        $json["input"]["post_titulo"] = $data["post_titulo"];
        $json["input"]["post_descricao"] = $data["post_descricao"];
        $json["image"]["post_image"] = $data["post_image"];
        
        echo json_encode($json);
    }
    
    public function ajax_get_membro_data() {

        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;
        $json["input"] = array();

        $this->load->model("time");

        $membro_id = $this->input->post("membro_id");
        $data = $this->time->get_data($membro_id)->result_array()[0];

        $json["input"]["membro_id"] = $data["membro_id"];
        $json["input"]["membro_nome"] = $data["membro_nome"];
        $json["image"]["membro_image"] = $data["membro_image"];
        // $json["input"]["membro_cidade"] = $data["membro_cidade"];
        $json["input"]["membro_cargo"] = $data["membro_cargo"];
        $json["input"]["membro_descricao"] = $data["membro_descricao"];
        
        echo json_encode($json);
    }

    public function ajax_list_posts() {
        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $this->load->model("posts");
        $posts = $this->posts->get_datatable();

        $data = array();
        foreach ($posts as $post) {
            $row = array();
            $row[] = $post->post_titulo;

            if ($post->post_image) {
                $row[] = '<img src="'.base_url().$post->post_image.'" style="max-width: 100px; max-height: 100px;">';
            } else {
                $row[] = "";
            }

            $row[] = '<div class="description">'.$post->post_descricao.'</div>';

            $row[] = '<div style="display: inline-block;">
                        <button class="btn btn-primary btn-edit-post" 
                            post_id="'.$post->post_id.'">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button class="btn btn-danger btn-del-post" 
                            post_id="'.$post->post_id.'">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>';

            $data[] = $row;
        }
        $json = array(
            "draw" => $this->input->post("draw"),
            "recordsTotal" => $this->posts->records_total(),
            "recordsFiltered" => $this->posts->records_filtered(),
            "data" => $data
        );

        echo json_encode($json);
    }

    public function ajax_list_membro() {
        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $this->load->model("time");
        $time = $this->time->get_datatable();

        $data = array();
        foreach ($time as $membro) {
            $row = array();
            $row[] = $membro->membro_nome;

            if ($membro->membro_image) {
                $row[] = '<img src="'.base_url().$membro->membro_image.'" style="max-width: 100px; 
                max-height: 100px; border-radius: 5px;">';
            } else {
                $row[] = "";
            }

            $row[] = $membro->membro_cidade;

            $row[] = $membro->membro_cargo;

            $row[] = '<div class="description">'.$membro->membro_descricao.'</div>';

            $row[] = '<div style="display: inline-block;">
                        <button class="btn btn-primary btn-edit-membro" 
                            membro_id="'.$membro->membro_id.'">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button class="btn btn-danger btn-del-membro" 
                            membro_id="'.$membro->membro_id.'">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>';

            $data[] = $row;
        }
        $json = array(
            "draw" => $this->input->post("draw"),
            "recordsTotal" => $this->time->records_total(),
            "recordsFiltered" => $this->time->records_filtered(),
            "data" => $data
        );

        echo json_encode($json);
    }

    public function ajax_list_users() {

        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $this->load->model("users");
        $users = $this->users->get_datatable();

        $data = array();
        foreach ($users as $user) {
            $row = array();
            $row[] = $user->user_full_name;

            $row[] = $user->user_login;

            $row[] = $user->user_email;

            $row[] = '<div style="display: inline-block;">
                        <button class="btn btn-primary btn-edit-user" 
                            user_id="'.$user->user_id.'">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button class="btn btn-danger btn-del-user" 
                            user_id="'.$user->user_id.'">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>';

            $data[] = $row;
        }
        $json = array(
            "draw" => $this->input->post("draw"),
            "recordsTotal" => $this->users->records_total(),
            "recordsFiltered" => $this->users->records_filtered(),
            "data" => $data
        );

        echo json_encode($json);
    }

    public function getCidades() {
        $uf_estado = $this->input->post("uf_estado");

        $this->load->model("cidades");

        echo $this->cidades->selectCidades($uf_estado);
    }
}
