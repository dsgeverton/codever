<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Cidades extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getByUfEstado($uf_estado) {
        $this->db->from("Municipio")
        ->where("Uf", $uf_estado)
        ->order_by("Nome");
        return $this->db->get();
    }

    public function selectCidades($uf_estado) {
        $options = "<option value=''>Selecione a cidade</option>";
        $cidades = $this->getByUfEstado($uf_estado);
        foreach($cidades -> result() as $cidade) {
            $options .= "<option value='{$cidade->Nome}'>{$cidade->Nome}</option>";
        }

        return $options;
    }
}