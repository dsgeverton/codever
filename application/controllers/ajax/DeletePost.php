<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DeletePost extends CI_Controller{

    public function ajax_delete_post_data() {

        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;

        $this->load->model("posts");
        $post_id = $this->input->post("post_id");
        $data = $this->posts->delete($post_id);

        echo json_encode($json);
    }

}