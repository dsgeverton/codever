<!-- Header -->
<header>
	<div class="container">
		<div class="slider-container">
			<div class="intro-text">
				<div class="intro-lead-in">Bem vindo ao nosso Site Exemplo!</div>
				<div class="intro-heading">É fácil visualizar como seu site ficará.</div>
				<a href="#about" class="page-scroll btn btn-xl">Mostre-me mais!</a>
			</div>
		</div>
	</div>
</header>

		<section id="about" class="light-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>SOBRE</h2>
							<p>A creative agency based on Candy Land, ready to boost your business with some beautifull templates. Lattes Agency is one of the best in town see more you will be amazed.</p>
						</div>
					</div>
				</div>
				<!-- end about module -->
			</div>
			<!-- /.container -->
		</section>
		
		<section class="overlay-dark bg-img1 dark-bg short-section">
			<div class="container text-center">
				<div class="row">
					<div class="col-md-3 mb-sm-30">
						<div class="counter-item">
							<h2 data-count="59">59</h2>
							<h6>awards</h6>
						</div>
					</div>
					<div class="col-md-3 mb-sm-30">
						<div class="counter-item">
							<h2 data-count="1054">3</h2>
							<h6>Clientes</h6>
						</div>
					</div>
					<div class="col-md-3 mb-sm-30">
						<div class="counter-item">
							<h2 data-count="34">2</h2>
							<h6>Time</h6>
						</div>
					</div>
					<div class="col-md-3 mb-sm-30">
						<div class="counter-item">
							<h2 data-count="154">2</h2>
							<h6>Projetos</h6>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="portfolio" class="light-bg">
			<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<div class="section-title">
						<h2>Posts / Eventos</h2>
						<p>Nosso portfólio é a melhor maneira de mostrar nosso trabalho, você pode ver aqui uma grande variedade do nosso trabalho.</p>
					</div>
				</div>
			</div>
			<div class="row">

				<?php if (!empty($posts)) {
					foreach ($posts as $post) { ?>
						<!-- start posts item -->
						<div class="col-md-4">
							<div class="ot-portfolio-item">
								<figure class="effect-bubba">
									<img src="<?=base_url().$post["post_image"]?>" alt="<?=$post["post_titulo"]?>" class="img-responsive center-block" />
									<figcaption>
										<h2><?=$post["post_titulo"]?></h2>
										<a href="#" data-toggle="modal" data-target="#post_<?=$post["post_id"]?>">View more</a>
									</figcaption>
								</figure>
							</div>
						</div>
						<!-- end posts item -->

								<!-- Modal for portfolio item 1 -->
						<div class="modal fade" id="post_<?=$post["post_id"]?>" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
								
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="Modal-label-1"><?=$post["post_titulo"]?></h4>
									</div>

									<div class="modal-body">
										<img src="<?=base_url().$post["post_image"]?>" alt="<?=$post["post_titulo"]?>" class="img-responsive" />
										<div class="modal-works"></div>
										<p><?=$post["post_descricao"]?></p>
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
									</div>
								</div>
							</div>
						</div>


					<?php } //FOREACH
				} else {// IF ?>
				<div class="col-lg-12 text-center">
						<p>Ainda não existe eventos cadastrados. <i class="fa fa-exclamation-circle"></i> </p>
				</div> <?php } ?>

			</div><!-- end container -->
		</section>
		<section id="team" class="light-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>Nosso time</h2>
							<p>Uma agência criativa baseada em novos designs e tecnologias, pronta para impulsionar seus negócios com alguns belos modelos!</p>
						</div>
					</div>
				</div>
				<div class="row">
					<!-- team member item -->
					<div class="col-md-3">
						<div class="team-item">
							<div class="team-image">
								<img src="<?php echo base_url(); ?>public/images/demo/author-2.jpg" class="img-responsive" alt="author">
							</div>
							<div class="team-text">
								<h3>TOM BEKERS</h3>
								<div class="team-location">Sydney, Australia</div>
								<div class="team-position">– CEO & Web Design –</div>
								<p>Mida sit una namet, cons uectetur adipiscing adon elit. Aliquam vitae barasa droma.</p>
							</div>
						</div>
					</div>
					<!-- end team member item -->
				</div>
			</div>
		</section>
		<section id="contact">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>Entre em Contato</h2>
							<p>Se você precisa tiver alguma dúvida, nós podemos ajudar! Entre em contato!<br>Nós podemos criar um novo visual para seus negócios.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<h3>Nossa Empresa</h3>
						<p>Rua ficticia, 432, Centro, Campos dos Goytacazes, Rio de Janeiro, Brasil</p>
						<p><i class="fa fa-phone"></i> +55 22 x xxxx - xxxx</p>
						<p><i class="fa fa-envelope"></i> contato@codever.com</p>
					</div>
					<div class="col-md-3">
						<h3>Horário de Funcionamento</h3>
						<p><i class="fa fa-clock-o"></i> <span class="day">Durante a Semana:</span><span>8h às 18h</span></p>
						<p><i class="fa fa-clock-o"></i> <span class="day">Sábado:</span><span>8h às 18h</span></p>
						<p><i class="fa fa-clock-o"></i> <span class="day">Domingo:</span><span>Fechado</span></p>
					</div>
					<div class="col-md-6">
						<form name="sentMessage" id="contactForm" novalidate="">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Seu Nome *" id="name" required="" data-validation-required-message="Please enter your name.">
										<p class="help-block text-danger"></p>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="email" class="form-control" placeholder="Seu Email *" id="email" required="" data-validation-required-message="Please enter your email address.">
										<p class="help-block text-danger"></p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<textarea class="form-control" placeholder="Mensagem *" id="message" required="" data-validation-required-message="Please enter a message."></textarea>
										<p class="help-block text-danger"></p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="row">
								<div class="col-lg-12 text-center">
									<div id="success"></div>
									<button type="submit" class="btn">Tudo certo, enviar!	</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
		<p id="back-top">
			<a href="#top"><i class="fa fa-angle-up"></i></a>
		</p>